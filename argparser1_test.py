
import argparse


def create_parser():
	parser = argparse.ArgumentParser()
	parser.add_argument('-i','--inputfile', type=str)
	return parser

def main():
	parser = create_parser()
	args = parser.parse_args() ; args = args.__dict__
	print(args["inputfile"])

if __name__ == "__main__": 
	main()

